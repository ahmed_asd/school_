# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import date, datetime
from dateutil.relativedelta import relativedelta


class Student(models.Model):
    _name = 'school.student'
    _description = 'school.members'

    name = fields.Char(string="Student Name")
    birth_date = fields.Date(string="Birth Date")
    age = fields.Integer(string="Age")
    marks = fields.Float(string="Total Marks")
    phone = fields.Char(string="Phone Number")
    color = fields.Integer()
    title = fields.Char(string="Title")

    #course
    course = fields.Many2many('school.course', string="Courses")

    #year
    yearr = fields.Many2one('school.year', string="Current Studying year")

    #skill
    skill = fields.Many2many('school.skill', string="Skills")

    @api.onchange('birth_date')
    def _compute_age(self):
        if self.birth_date:
            self.age = relativedelta(date.today(), self.birth_date).years



class Course(models.Model):
    _name = 'school.course'
    _description = 'school.material'

    course_name = fields.Char(string="Course Name")

    course_mark = fields.Integer(string="Course Marks")


#year model


class Year(models.Model):
    _name = 'school.year'
    _description = 'school.studying.year'

    name = fields.Char(string="Studying year")

    student_ = fields.One2many('school.student', 'yearr')


class Skill(models.Model):
    _name = 'school.skill'
    _description = 'school.skill'

    name = fields.Char(string="Skill name")

